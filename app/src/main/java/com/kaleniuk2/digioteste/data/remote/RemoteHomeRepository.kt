package com.kaleniuk2.digioteste.data.remote

import com.kaleniuk2.digioteste.core.ResultWrapper
import com.kaleniuk2.digioteste.core.createWebService
import com.kaleniuk2.digioteste.core.makeRequest
import com.kaleniuk2.digioteste.data.HomeRepository
import com.kaleniuk2.digioteste.data.model.Products

class RemoteHomeRepository() : HomeRepository {
    override suspend fun getProducts(): ResultWrapper<Products> {
        return makeRequest { createWebService<HomeService>().getProducts() }
    }

}
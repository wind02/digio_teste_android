package com.kaleniuk2.digioteste.data

import com.kaleniuk2.digioteste.core.ResultWrapper
import com.kaleniuk2.digioteste.data.model.Products

interface HomeRepository {
    suspend fun getProducts(): ResultWrapper<Products>
}
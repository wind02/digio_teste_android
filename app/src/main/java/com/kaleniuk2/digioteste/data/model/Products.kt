package com.kaleniuk2.digioteste.data.model

import com.google.gson.annotations.SerializedName

data class Products(
    @SerializedName("spotlight")
    val spotlight: List<Spotlight>,
    @SerializedName("products")
    val products: List<Product>,
    @SerializedName("cash")
    val cash: Cash
)

data class Spotlight(
    @SerializedName("name")
    val name: String,
    @SerializedName("bannerURL")
    val bannerURL: String,
    @SerializedName("description")
    val description: String
)

data class Product(
    @SerializedName("name")
    val name: String,
    @SerializedName("imageURL")
    val imageURL: String,
    @SerializedName("description")
    val description: String
)

data class Cash(
    @SerializedName("title")
    val title: String,
    @SerializedName("bannerURL")
    val bannerURL: String,
    @SerializedName("description")
    val description: String
)
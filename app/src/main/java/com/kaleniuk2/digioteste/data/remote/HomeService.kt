package com.kaleniuk2.digioteste.data.remote

import com.kaleniuk2.digioteste.data.model.Products
import retrofit2.http.GET

interface HomeService {
    @GET("products")
    suspend fun getProducts() : Products
}
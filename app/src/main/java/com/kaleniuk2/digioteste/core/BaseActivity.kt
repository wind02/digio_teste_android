package com.kaleniuk2.digioteste.core

import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_home.*

open class BaseActivity : AppCompatActivity() {
    protected fun showToast(text: String) =
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()

    protected fun showLoading(show: Boolean) {
        progressMain.visibility = if (show) { View.VISIBLE } else { View.GONE }
    }

}
package com.kaleniuk2.digioteste.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.kaleniuk2.digioteste.R
import com.kaleniuk2.digioteste.data.model.Spotlight

class SpotlightAdapter(private val spotlighList: List<Spotlight>) : PagerAdapter() {
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount() = spotlighList.size

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(container.context)
            .inflate(R.layout.item_spotlight, container, false)
        val ivSpotlight = view.findViewById<ImageView>(R.id.ivSpotlight)

        Glide.with(container.context).load(spotlighList[position].bannerURL).into(ivSpotlight)
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}

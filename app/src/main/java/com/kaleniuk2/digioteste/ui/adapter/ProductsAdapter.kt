package com.kaleniuk2.digioteste.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kaleniuk2.digioteste.R
import com.kaleniuk2.digioteste.data.model.Product

class ProdutsAdapter(private val listProducts: List<Product>)
    : RecyclerView.Adapter<ProductViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        return ProductViewHolder(LayoutInflater.from(parent.context).
        inflate(R.layout.item_product, parent, false))
    }

    override fun getItemCount() = listProducts.size

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(listProducts[position])
    }
}

class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
    private val ivProduct: ImageView = itemView.findViewById<ImageView>(R.id.ivProduct)

    fun bind(product: Product) {
        Glide.with(itemView.context).load(product.imageURL).into(ivProduct)
    }
}



package com.kaleniuk2.digioteste.ui

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.kaleniuk2.digioteste.R
import com.kaleniuk2.digioteste.core.BaseActivity
import com.kaleniuk2.digioteste.data.model.Cash
import com.kaleniuk2.digioteste.data.model.Product
import com.kaleniuk2.digioteste.data.model.Products
import com.kaleniuk2.digioteste.data.model.Spotlight
import com.kaleniuk2.digioteste.ui.adapter.ProdutsAdapter
import com.kaleniuk2.digioteste.ui.adapter.SpotlightAdapter
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity() {
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        createViewModelInstance()
        setObservers()
        getProducts()
    }

    private fun createViewModelInstance() {
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
    }

    private fun getProducts() {
        homeViewModel.getProducts()
    }

    private fun setObservers() {
        homeViewModel.homeState.observe(this, Observer {
            when(it) {
                is HomeState.ShowLoading -> showLoading(true)
                is HomeState.HideLoading -> showLoading(false)
                is HomeState.SuccessList -> configViews(it.products)
                is HomeState.Error -> createTryAgainDialog(it.error)
            }
        })
    }

    private fun createTryAgainDialog(error: String) {
        AlertDialog.Builder(this)
            .setTitle(error)
            .setPositiveButton(R.string.try_again
            ) { dialogInterface, _ ->
                getProducts()
                dialogInterface.dismiss()
            }.setNegativeButton(R.string.cancel) { dialogInterface, _ ->
                dialogInterface.dismiss()
            }.show()
    }

    private fun configViews(products: Products) {
        configPageAdapter(products.spotlight)
        configCash(products.cash)
        configRecycler(products.products)
    }

    private fun configCash(cash: Cash) {
        tvCash.text = cash.title
        Glide.with(this).load(cash.bannerURL).into(ivCashItem)
    }

    private fun configPageAdapter(listSpotlight: List<Spotlight>) {
        vpHome.adapter = SpotlightAdapter(listSpotlight)
    }

    private fun configRecycler(listProducts: List<Product>)  {
        recyclerProducts.also {
            it.adapter = ProdutsAdapter(listProducts)
            it.layoutManager =
                LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        }
    }

}
package com.kaleniuk2.digioteste.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kaleniuk2.digioteste.core.ResultWrapper
import com.kaleniuk2.digioteste.data.HomeRepository
import com.kaleniuk2.digioteste.data.model.Products
import com.kaleniuk2.digioteste.data.remote.RemoteHomeRepository
import kotlinx.coroutines.launch

class HomeViewModel(private val repository: HomeRepository = RemoteHomeRepository()) : ViewModel() {
    private val _homeState: MutableLiveData<HomeState> = MutableLiveData()
    val homeState: LiveData<HomeState> = _homeState

    fun getProducts() {
        viewModelScope.launch {
            _homeState.value = HomeState.ShowLoading
            when (val result = repository.getProducts()) {
                is ResultWrapper.Success -> _homeState.value = HomeState.SuccessList(result.value)
                is ResultWrapper.GenericError -> _homeState.value = HomeState.Error(ERROR_REQUEST)
                is ResultWrapper.NetworkError -> _homeState.value = HomeState.Error(ERROR_REQUEST)
            }
            _homeState.value = HomeState.HideLoading
        }
    }

    companion object {
        private const val ERROR_REQUEST = "Erro ao consultar produtos"
    }
}

sealed class HomeState {
    object ShowLoading : HomeState()
    object HideLoading : HomeState()
    class SuccessList(val products: Products): HomeState()
    class Error(val error: String): HomeState()
}
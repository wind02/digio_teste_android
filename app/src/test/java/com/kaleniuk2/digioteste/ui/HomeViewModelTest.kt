package com.kaleniuk2.digioteste.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.kaleniuk2.digioteste.core.ResultWrapper
import com.kaleniuk2.digioteste.data.HomeRepository
import com.kaleniuk2.digioteste.data.model.Cash
import com.kaleniuk2.digioteste.data.model.Product
import com.kaleniuk2.digioteste.data.model.Products
import com.kaleniuk2.digioteste.data.model.Spotlight
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.isA
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class HomeViewModelTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()
    private val dispatcher = TestCoroutineDispatcher()

    @Mock
    private lateinit var viewStateObserver: Observer<HomeState>

    @Before
    fun setup() {
        Dispatchers.setMain(dispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `when get products with success should update live data with products list`() {
        //given
        val mockProducts = createProductsMock()
        val mockSuccessResponse = ResultWrapper.Success(mockProducts)
        val repositoryMock = HomeRepositoryMock(mockSuccessResponse)
        val viewModel = HomeViewModel(repositoryMock)
        viewModel.homeState.observeForever(viewStateObserver)

        //when
        viewModel.getProducts()

        //then
        verify(viewStateObserver).onChanged(HomeState.ShowLoading)
        verify(viewStateObserver).onChanged(isA(HomeState.SuccessList::class.java))
        verify(viewStateObserver).onChanged(HomeState.HideLoading)

    }

    @Test
    fun `when get products with network error should update live data with error`() {
        //given
        val mockError = ResultWrapper.NetworkError
        val repositoryMock = HomeRepositoryMock(mockError)
        val viewModel = HomeViewModel(repositoryMock)
        viewModel.homeState.observeForever(viewStateObserver)

        //when
        viewModel.getProducts()

        //then
        verify(viewStateObserver).onChanged(HomeState.ShowLoading)
        verify(viewStateObserver).onChanged(isA(HomeState.Error::class.java))
        verify(viewStateObserver).onChanged(HomeState.HideLoading)
    }

    @Test
    fun `when get products with generic error should update live data with error`() {
        //given
        val mockError = ResultWrapper.GenericError(404, "erro")
        val repositoryMock = HomeRepositoryMock(mockError)
        val viewModel = HomeViewModel(repositoryMock)
        viewModel.homeState.observeForever(viewStateObserver)

        //when
        viewModel.getProducts()

        //then
        verify(viewStateObserver).onChanged(HomeState.ShowLoading)
        verify(viewStateObserver).onChanged(isA(HomeState.Error::class.java))
        verify(viewStateObserver).onChanged(HomeState.HideLoading)
    }

    private fun createProductsMock(): Products {
        return Products(
            listOf(Spotlight("mock", "mock", "mock")),
            listOf(Product("mock", "mock", "mock")),
            Cash("mock", "Mock", "mock")
        )

    }

    class HomeRepositoryMock(private val result: ResultWrapper<Products>): HomeRepository {
        override suspend fun getProducts() = result
    }

}